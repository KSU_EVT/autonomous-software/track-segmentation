# U-Net Model for Track Segmentation

I used [this](https://www.tensorflow.org/tutorials/images/segmentation) tensorflow tutorial to train the data

Data is labeled using [labelme](https://github.com/wkentaro/labelme) as polygons called "road" then I export it in the voc format to train.

Install the latest version of [Tensorflow 2](https://www.tensorflow.org/install/pip#step-by-step_instructions) to use this.
I would also recommend you use this inside of a linux environment since it is annoying to setup GPU training with Windows wsl.

To train simply run: `python ./train.py`

And to test your model run: `python ./test.py`  or `python ./test.py -i image_name.png`