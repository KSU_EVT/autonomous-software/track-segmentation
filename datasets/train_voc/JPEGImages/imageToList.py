import os

# Set the directory path where the images are stored
# I'd recommend you keep this in the JPEGImages folder.  
dir_path = os.getcwd()

# Split images to train and val
with open("train.txt", "w") as f:
    for i in range(0, len(os.listdir(dir_path)), 2):
        file_name = os.listdir(dir_path)[i]
        if file_name.endswith(".jpg") or file_name.endswith(".png"):
            f.write("%s\n" % os.path.splitext(file_name)[0])

with open("val.txt", "w") as f:
    for i in range(1, len(os.listdir(dir_path)), 2):
        file_name = os.listdir(dir_path)[i]
        if file_name.endswith(".jpg") or file_name.endswith(".png"):
            f.write("%s\n" % os.path.splitext(file_name)[0])