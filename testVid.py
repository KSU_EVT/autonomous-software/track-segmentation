from PIL import Image
import numpy as np
import cv2
import tensorflow as tf
from keras.models import load_model
import copy
import os

INPUT_SHAPE = [1920, 1024, 3]

dir_path = os.path.dirname(os.path.realpath(__file__))

# Specify model name and load model
model_path = os.path.join(dir_path, 'logs\\final-model.h5')
model = load_model(model_path, compile=False)
model.compile(
    optimizer='adam',
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy'])
print('{} model loaded.'.format(model_path))

colors = [(0, 0, 0), (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128),
          (128, 0, 128), (0, 128, 128), (128, 128, 128),
          (64, 0, 0), (192, 0, 0), (64, 128, 0), (192, 128, 0), (64, 0, 128),
          (192, 0, 128), (64, 128, 128), (192, 128, 128), (0, 64, 0),
          (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128),
          (128, 64, 12)]

def cvtColor(image):
    if len(np.shape(image)) == 3 and np.shape(image)[-2] == 3:
        return image
    else:
        image = image.convert('RGB')
        return image
    
def resize_image(image, size):
    iw, ih = image.size
    w, h = size

    scale = min(w / iw, h / ih)
    nw = int(iw * scale)
    nh = int(ih * scale)

    image = image.resize((nw, nh), Image.BICUBIC)
    new_image = Image.new('RGB', size, (128, 128, 128))
    new_image.paste(image, ((w - nw) // 2, (h - nh) // 2))

    return new_image, nw, nh

def normalize(image):
    image = image / 127.5 - 1
    return image

def detect_video(video_path):
    cap = cv2.VideoCapture(video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('output.mp4', fourcc, fps, (width, height))

    while True:
        ret, frame = cap.read()
        if not ret:
            break
        image = Image.fromarray(frame)
        image = cvtColor(image)
        old_img = copy.deepcopy(image)
        ori_h = np.array(image).shape[0]
        ori_w = np.array(image).shape[1]

        image_data, nw, nh = resize_image(image, (INPUT_SHAPE[1], INPUT_SHAPE[0]))

        image_data = normalize(np.array(image_data, np.float32))

        image_data = np.expand_dims(image_data, 0)

        pr = model.predict(image_data)[0]

        pr = pr[int((INPUT_SHAPE[0] - nh) // 2) : int((INPUT_SHAPE[0] - nh) // 2 + nh), \
                int((INPUT_SHAPE[1] - nw) // 2) : int((INPUT_SHAPE[1] - nw) // 2 + nw)]

        pr = cv2.resize(pr, (ori_w, ori_h), interpolation=cv2.INTER_LINEAR)

        pr = pr.argmax(axis=-1)

        seg_img = np.reshape(
            np.array(colors, np.uint8)[np.reshape(pr, [-1])], [ori_h, ori_w, -1])

        image = Image.fromarray(seg_img)
        image = Image.blend(old_img, image, 0.7)
        out.write(np.array(image))

    cap.release()
    out.release()
    cv2.destroyAllWindows()

test_video_path = 'D:\\uh yolo stuff\\objDetection\\\images\\testVid.mp4'

detect_video(test_video_path)